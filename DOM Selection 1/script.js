// DOM Selection
// document.getElemenById() -> element
const judul = document.getElementById('judul');
judul.style.color = 'red';
judul.style.backgroundColor = '#ccc';
judul.innerHTML = 'Risky Noviyanti';

// document.getElemenByTagName()
// -> HTMCollections
const p = document.getElementsByTagName('p');

for( let i = 0; i < p.length; i ++){
    p[0].style.backgroundColor = 'lightblue';
}

const h1 = document.getElementsByTagName('h1')[0];
h1.style.fontSize = '50px';


// document.getElementsByClassName()
// -> HTMLCollection
const pi = document.getElementsByClassName('p1')[0];
p1.innerHTML = 'Ini diubah dari javascript';